# XNAT multi project

This repository organizes several XNAT related projects into one "multi" project for the convenience of development and debugging.
"Multi" in quotes because subprojects are built with Maven and Gradle. For the time being solution is to organize this project as Gradle one and run Maven subprojects using command line task.
It is not perfect, but it works. :)

This project was used to modify the XNAT code to allow addition of DICOM modalities (which are missing in the core) through plugins.
As an example the `px-session` project here is a XNAT plugin which adds the PX modality.

**Note!** It may sound obvious, but make sure to compile the project with JDK 1.7 or 1.8, because later versions may cause errors.

For some optimization of the build process here is a sample in the `sample.gradle.properties` file.
Modify it as you need and rename to `gradle.properties`.
